const Discord = require('discord.io');
const logger = require('winston');
const auth = require('./auth.json');
const pikudHaoref = require('pikud-haoref-api');
const fs = require('fs');
const _ = require('lodash');


let config = {alertSubs: {}};
// Set polling interval in millis
let interval = 5000;

// Define polling function
const poll = function (lastAlertZones) {
    // Get currently active rocket alert zones as an array of zone codes
    // Example response: ["גולן 1", "חיפה 75", "שפלה 182"]
    pikudHaoref.getActiveRocketAlertZones(function (err, alertZones) {
        // Schedule polling in X millis
        setTimeout(() => poll(alertZones), interval);

        // Log errors
        if (err) {
            return console.error('Retrieving active rocket alert zones failed: ', err);
        }
        alertZones.forEach(zone => {
            if (lastAlertZones.indexOf(zone) === -1) {
                sendAlert(zone);
            }
        });

    });
};
const saveConfig = function () {
    // console.log(config);
    fs.writeFile('config.json', JSON.stringify(config), 'utf8', function (err) {
        if (err) throw err;
    });
};
const sendAlert = function (alertZone) {
    Object.keys(config.alertSubs).forEach(channelID => {
        if (Object.keys(config.alertSubs[channelID]).indexOf(alertZone) > -1) {
            bot.sendMessage({
                to: channelID,
                message: 'Rocket alert for region: ' + alertZone + ' ' + config.alertSubs[channelID][alertZone].join(', ')
            });
        }
    });
};
// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
let bot = new Discord.Client({
    token: auth.token,
    autorun: true,
});
bot.on('ready', function () {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
    bot.setPresence({game: {type: 3, name: 'the skies'}});

    const file = 'config.json';

    fs.open(file, 'r', (err, fd) => {
        if (err) {
            if (err.code === 'ENOENT') {
                fs.writeFile(file, JSON.stringify(config), 'utf8', function (err) {
                    if (err) throw err;
                });
                return;
            }
            throw err;
        }
        fs.readFile(fd, 'utf8', (err, data) => {
            if (err) throw err;
            config = JSON.parse(data);
            // console.log(config, data);
        });
    });


    // Start polling for active alert zones
    poll();

});
bot.on('message', function (user, userID, channelID, message) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (message.substring(0, 1) === '!') {
        let args = message.substring(1).split(' ');
        let cmd = args[0];

        // console.log(args);
        args = args.splice(1);
        switch (cmd) {
            // !ping
            case 'ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
                break;
            case 'hfc':
                // console.log(args);

                let subcmd = args[0];
                args = args.splice(1);
                switch (subcmd) {
                    case 'addalert':
                        // console.log(args, config);
                        let tag = args[0];
                        let region = args.splice(1).join(' ');

                        let regionTags = _.get(config, ['alertSubs', channelID, region], []);
                        if (regionTags.indexOf(tag) === -1) {
                            regionTags.push(tag);
                        }
                        _.set(config, ['alertSubs', channelID, region], regionTags);
                        saveConfig();
                        break;
                }
                break;
        }
    }
});
